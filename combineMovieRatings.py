from pymongo import MongoClient
import copy
client = MongoClient('localhost',3001)
db = client.meteor

#averageScoreByGender={'M':0,'F':0}
occupationList=[u'administrator', u'writer', u'retired', u'student', u'entertainment', u'marketing', u'executive', u'none', u'scientist', u'healthcare', u'other', u'lawyer', u'educator', u'technician', u'librarian', u'programmer', u'artist', u'salesman', u'doctor', u'homemaker', u'engineer']

movieList=[]
score={'1':[],'2':[],'3':[],'4':[],'5':[]}
uploadScore={'1':None,'2':None,'3':None,'4':None,'5':None}
cursor=db.items.find()
for doc in cursor:
	movieList.append(doc['movieId'])
for movie in movieList:
	movieScore=copy.deepcopy(score)	
	cursor=db.ratings.find({'itemId':movie})
	for item in cursor:
		movieScore[str(item['rating'])].append(item['userId'])
	movieUploadScore=copy.deepcopy(uploadScore)
	for key in movieScore:
		movieScoreByOccupation={}
		for occupation in occupationList:
			movieScoreByOccupation[occupation]=0
		movieScoreByZip=[]
		movieScoreBySex={'M':0,'F':0}
		for users in movieScore[key]:
			cursor=db.user.find({'userId':users})
			for item in cursor:
				movieScoreBySex[item['gender']]+=1
				movieScoreByOccupation[item['occupation']]+=1
				movieScoreByZip.append(item['zip'])
		movieUploadScore[key]={'sex':movieScoreBySex,'occupation':movieScoreByOccupation,'zip':movieScoreByZip}
	movieUploadScore['movieId']=movie
	db.movieScoreForPieChart.insert(movieUploadScore)


