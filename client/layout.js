$(document).ready(function() {
    $('[data-toggle="offcanvas"]').click(function() {
        $('.row-offcanvas').toggleClass('active')
    });
});



Router.configure({
    loadingTemplate: 'loading',
});


Router.route('home', {
  path: '/',
  layoutTemplate: 'layout',
  yieldTemplates: {
    'mainview': {to: 'view'},
  }
});
