// collections are declared here
//access from both server side and client side.

Collection={};
Collection.ratings = new Meteor.Collection('ratings')
Collection.items= new Meteor.Collection('items')
Collection.user= new Meteor.Collection('user')
Collection.userScoreForPieChart=new Meteor.Collection('userScoreForPieChart')
Collection.movieScoreForPieChart= new Meteor.Collection('movieScoreForPieChart')
Collection.zipcode= new Meteor.Collection('zipcode')
/*
Collection.mc1=new Meteor.Collection('mc1')
Collection.mdsTianyu=new Meteor.Collection('mdsTianyu')
Collection.mds=new Meteor.Collection('mds')
Collection.mdscat=new Meteor.Collection('mds6cat')
Collection.timeline=new Meteor.Collection('timeline')
Collection.lineChart=new Meteor.Collection('lineChart')
Collection.timeInterval= new Meteor.Collection('timeInterval')
Collection.checkinLineChart= new Meteor.Collection('checkinLineChart')
Collection.mdsFilter= new Meteor.Collection('mdsFilter')
Collection.mdsConn= new Meteor.Collection('mdsConn')
Collection.mdsFromConn= new Meteor.Collection('mdsFromConn')
Collection.checkinById = new Meteor.Collection('checkinById')
Collection.mc2=new Meteor.Collection('mc2')
Collection.heatmapPerDay=new Meteor.Collection('heatmapPerDay')
Collection.heatmap_ids= new Meteor.Collection('heatmap_ids');
Collection.inout= new Meteor.Collection('inout')
Collection.friTheme= new Meteor.Collection('friTheme')
Collection.satTheme= new Meteor.Collection('satTheme')
Collection.sunTheme= new Meteor.Collection('sunTheme')
*/