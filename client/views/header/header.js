Template.header.helpers({
    activeRouteClass: function(/* route names */) {
        var args = Array.prototype.slice.call(arguments, 0);
        args.pop();
    
        var active = _.any(args, function(name) {
            return Router.current() && Router.current().route.name === name
        });
    
        return active && 'active';
    },
});

Template.header.events({
	'click #clicklogo': function(){
		Router.go('/part2');
		console.log('clicked logo')
	}
})