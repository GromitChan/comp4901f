bipartite= function(canvasG,width,height,items,ratings,userGroup1,userGroup2) {  

  var data = [];
  var userCoord1={};
  var userCoord2={};
  var itemCoord={};

  if((userGroup1.length<items.length)&&(userGroup2.length<items.length)){
    console.log('case 1')
    var ratio1=items.length/((userGroup1.length));
    var ratio2=items.length/((userGroup2.length));
    for(var i=0;i<userGroup1.length;i++){
      userGroup1[i]['coord']=[0,i*ratio1*2]
      userCoord1[userGroup1[i]['userId']]=userGroup1[i]['coord']
    }
    for(var i=0;i<userGroup2.length;i++){
      userGroup2[i]['coord']=[2,i*ratio2*2]
      userCoord2[userGroup2[i]['userId']]=userGroup2[i]['coord']
    }
    for(var i=0;i<items.length;i++){
      items[i]['coord']=[1,i*2]
      itemCoord[items[i]['movieId']]=items[i]['coord']
    }
  }else{
    if(userGroup1.length>userGroup2.length){
      console.log('case 2')
      if(items.length>0){
        var ratio1=userGroup1.length/((items.length));
      }else{
        var ratio1=0;
      }
      var ratio2=userGroup1.length/((userGroup2.length));

      for(var i=0;i<userGroup1.length;i++){
        userGroup1[i]['coord']=[0,i*2]
        userCoord1[userGroup1[i]['userId']]=userGroup1[i]['coord']
      }
      for(var i=0;i<userGroup2.length;i++){
        userGroup2[i]['coord']=[2,i*ratio2*2]
        userCoord2[userGroup2[i]['userId']]=userGroup2[i]['coord']
      }
      for(var i=0;i<items.length;i++){
        items[i]['coord']=[1,i*ratio1*2]
        itemCoord[items[i]['movieId']]=items[i]['coord']
      }
    }else{
      console.log('case 3')
      console.log(userGroup1)
      var ratio1=userGroup2.length/((userGroup1.length));
      if(items.length>0){
        var ratio2=userGroup2.length/((items.length));
      }else{
        var ratio2=0;
      }
      for(var i=0;i<userGroup1.length;i++){
        userGroup1[i]['coord']=[0,i*2*ratio1]
        userCoord1[userGroup1[i]['userId']]=userGroup1[i]['coord']
      }
      for(var i=0;i<userGroup2.length;i++){
        userGroup2[i]['coord']=[2,i*2]
        userCoord2[userGroup2[i]['userId']]=userGroup2[i]['coord']
      }
      for(var i=0;i<items.length;i++){
        items[i]['coord']=[1,i*ratio2*2]
        itemCoord[items[i]['movieId']]=items[i]['coord']
      }
    }
  }
        //console.log(userGroup1)
        var ratingsLine=[];
        for(var i=0;i<ratings.length;i++){
          var temp={}
          if((ratings[i]['userId'] in userCoord1)&&(ratings[i]['itemId'] in itemCoord)){
            temp['coord']=[userCoord1[ratings[i]['userId']],itemCoord[ratings[i]['itemId']]]
            temp['rating']=ratings[i]['rating']
            ratingsLine.push(temp)
          }
          if((ratings[i]['userId'] in userCoord2)&&(ratings[i]['itemId'] in itemCoord)){
            temp['coord']=[userCoord2[ratings[i]['userId']],itemCoord[ratings[i]['itemId']]]
            temp['rating']=ratings[i]['rating']
            ratingsLine.push(temp)
          }

        }
        var x = d3.scale.linear()
        .domain([0, 2])
        .range([ 0, width*0.9 ]);
        var maxUser= Math.max(d3.max(userGroup1, function(d) { return d['coord'][1]; }),d3.max(userGroup2, function(d) { return d['coord'][1]; }))
        var y = d3.scale.linear()
        .domain([0, Math.max(maxUser,d3.max(items, function(d) { return d['coord'][1]; }))])
        .range([ height*0.9, 0 ]);

        var chart = d3.select(canvasG)
        .append('svg')
        .attr("id","biGraphSvg")
        .attr('width', width )
        .attr('height', height)
        .attr('class', 'chart')

        var main = chart.append('g')
        .attr('transform', 'translate(' + width*0.05 + ','+width*0.01+')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main')   

      // draw the x axis
      /*
      var xAxis = d3.svg.axis()
  	.scale(x)
  	.orient('bottom');
    
      main.append('g')
  	.attr('transform', 'translate(0,' + height + ')')
  	.attr('class', 'main axis date')
  	.call(xAxis);
    */
      // draw the y axis
      /*
      var yAxis = d3.svg.axis()
  	.scale(y)
  	.orient('left');
    
      main.append('g')
  	.attr('transform', 'translate(0,0)')
  	.attr('class', 'main axis date')
  	.call(yAxis);
    */
    var g = main.append("svg:g"); 


      //console.log(ratingsLine)
      color=['#ea0001','#fa9924','#feC923','#92d14f','#00602b']
      g.selectAll("scatter-line")
      .data(ratingsLine)
      .enter()
      .append("line")          // attach a line
      .attr("class", "trendline")
    .style("stroke", function(d){return color[d['rating']-1]})  // colour the line
    .attr("x1", function(d){return x(d['coord'][0][0]);})     // x position of the first end of the line
    .attr("y1", function(d){return y(d['coord'][0][1]);})      // y position of the first end of the line
    .attr("x2", function(d){return x(d['coord'][1][0]);})      // x position of the second end of the line
    .attr("y2", function(d){return y(d['coord'][1][1]);})    // y position of the second end of the line
    .attr("stroke-width", 1)
    .attr("opacity",0);

      var userDots1=g.selectAll("user1-dots")
      .data(userGroup1)
      .enter().append("g")
      .on('click',function(d){
       // console.log(d.userId)
        Session.set('userSelectedForPieChart',d.userId)
        Session.set('userOccupationSelectedForPieChart',d.occupation)
      })
      .on("mouseover", function(d){
        d3.select(this).select("circle").transition()
      .attr("r", 3)
      d3.select(this).select('text').transition()
      .style("font-size","15px")
      })
      .on("mouseout", function(d){
        d3.select(this).select("circle").transition()
      .attr("r", 1)
      d3.select(this).select('text').transition()
      .style("font-size","5px")
      })
      .attr("transform", function(d){return "translate("+x(d['coord'][0])+","+y(d['coord'][1])+")"});;

      userDots1
      .append("svg:circle")
      .attr("r", 1);
      userDots1
      .append("text")
      .attr("dx", 12)
      .attr("dy", "0.35em")
      .style("font-size","5px")
      .text(function(d) { return d.gender });

      var userDots2=g.selectAll("user2-dots")
      .data(userGroup2)
      .enter().append("g")
      .on('click',function(d){
        //console.log(d.userId)
        Session.set('userSelectedForPieChart',d.userId)
        Session.set('userOccupationSelectedForPieChart',d.occupation)
      })
      .on("mouseover", function(d){
        d3.select(this).select("circle").transition()
      .attr("r", 3)
      d3.select(this).select('text').transition()
      .style("font-size","15px")
      })
      .on("mouseout", function(d){
        d3.select(this).select("circle").transition()
      .attr("r", 1)
      d3.select(this).select('text').transition()
      .style("font-size","5px")
      })
      .attr("transform", function(d){return "translate("+x(d['coord'][0])+","+y(d['coord'][1])+")"});;

      userDots2
      .append("svg:circle")
      .attr("r", 1);
      userDots2
      .append("text")
      .attr("dx", 12)
      .attr("dy", "0.35em")
      .style("font-size","5px")
      .text(function(d) { return d.gender });

    var dots=g.selectAll("items-dots")
    .data(items)
    .enter().append("g")
    .on('click',function(d){
        Session.set('movieSelectedForPieChart',d.movieId)
      })
    .on("mouseover", function(d){
      d3.select(this).select("circle").transition()
      .attr("r", 3)
      d3.select(this).select('text').transition()
      .style("font-size","15px")
    })
    .on("mouseout", function(d){
      d3.select(this).select("circle").transition()
      .attr("r", 1)
      d3.select(this).select('text').transition()
      .style("font-size","5px")
    })
    .attr("transform", function(d){return "translate("+x(d['coord'][0])+","+y(d['coord'][1])+")"});
    dots.append("svg:circle")
            //.attr("cx", function (d,i) { return x(d['coord'][0]); } )
            //.attr("cy", function (d) { return y(d['coord'][1]); } )
            .attr("r", 1);
            dots.append("text")
            .attr("dx", 12)
            .attr("dy", "0.35em")
            .style("font-size","5px")
            .text(function(d) { return d.movieTitle });

          }
