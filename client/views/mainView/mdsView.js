d3.mdsView= function() {
    var width, height;

    function mdsView(g, data) {

        var conf=Template.mainview.configure.mdsView;
        // var col=conf.col, row=conf.row;

        _.each(data, function(d){
            d.coord=JSON.parse(d.coord)
        })
        var dataToUse=data.slice(0);
        var mdsFilter=Session.get('mdsFilterVisitors');
        var mdsFilterTimeInterval=Session.get('mdsFilterTimeInterval');

        var toggle=Session.get('toggleMds')
        var overlap=Session.get('overlap')
/*
        var thrillRideMdsFilterIds=Session.get('thrillRideMdsFilterIds');
        var hasSetThrillRideFilter=Session.get('hasSetThrillRideFilter');

        var kiddieRideMdsFilterIds=Session.get('kiddieRideMdsFilterIds');
        var hasSetKiddieRideFilter=Session.get('hasSetKiddieRideFilter');

        console.log(hasSetThrillRideFilter)
        console.log(hasSetKiddieRideFilter)
        */

        if(mdsFilter){
            dataToUse= dataToUse.filter(function(d){
                return mdsFilter.indexOf(d.category) ==-1;
            })
        }
        dict={}
        for(var i=0;i<dataToUse.length;i++){
            tmp=dataToUse[i]['coord'][0].toString()+','+dataToUse[i]['coord'][1].toString()
            if (tmp in dict){
                dict[tmp].push(dataToUse[i])
            }else{
                dict[tmp]=[dataToUse[i]]
            }
        }
      //  console.log(dict)
        dataToUse=[]
        for(key in dict){
            if (dict[key].length>overlap){
                for (var k=0;k<dict[key].length;k++){
                    dataToUse.push(dict[key][k])
                }
            }
        }
        var xDomain, yDomain;
        xDomain=d3.extent(data, function(d){
            return d.coord[0];
        })
        yDomain=d3.extent(data, function(d){
            return d.coord[1];
        })

        var xScale = d3.scale.linear().
                domain(xDomain)
                .range([conf.padding, width - conf.padding]),

            yScale = d3.scale.linear().
                domain(yDomain)
                .range([conf.padding, height-conf.padding]);


        g.selectAll('circle')
            .data(dataToUse)
            .enter()
                .append('circle')
                .attr('class', 'mdsView')
                .attr('cx', function(d){
                    return xScale(d.coord[0]);
                })
                .attr('cy', function(d){
                    return yScale(d.coord[1]);
                })
                .attr('r', function(d){
                    return 3;
                })
                .attr('fill', function(d){
                    if(d.category==1){
                        return '#66c2a5';
                    }else if(d.category==2){
                        return '#8da0cb';
                    }else if(d.category==3){
                        return '#fc8d62';
                    }else{
                        return '#000'
                    }
                })
                .on('click', function(d){
                    Router.go('/part2')
                })
                // .attr('fill', '#74add1')
                // .attr('opacity', 0.5)

    }


    mdsView.width = function(x) {
        var res;
        if (!arguments.length) {
            res = width;
        } else {
            width = x;
            res = mdsView;
        }
        return res;
    };

    mdsView.height = function(x) {
        var res;
        if (!arguments.length) {
            res = height;
        } else {
            height = x;
            res = mdsView;
        }
        return res;
    };


    return mdsView;
}
