Template.mainview.rendered = function() {



    document.getElementById('middleContainerBody').setAttribute('style','height:'+($('#middleContainer').width()*0.65).toString()+'px; overflow-y: scroll;');
    $('#mapContainer').height($('#rightBottomContainer').width()/2)
    var map = new Datamap({
        element: document.getElementById('mapContainer'),
        scope: 'usa',
        fills:{
            defaultFill: '#d3d3d3'
        }
    });
    $('#MovieDescriptionList').height($('#pieChart3').width())
    $('#pieChart1').height($('#pieChart1').width())    
    $('#pieChart2').height($('#pieChart2').width())  
    $('#pieChart3').height($('#pieChart3').width())    
    $('#pieChart4').height($('#pieChart4').width()) 
    $('#pieChart5').height($('#pieChart5').width())
    var pieChart1 = c3.generate({
    bindto: '#pieChart1',
    data: {
        // iris data from R
        columns: [
            ['No User', 30]
        ],
        type : 'pie',
        onclick: function (d, i) { Session.set('selectedRating',d.id) },
        onmouseover: function (d, i) {  },
        onmouseout: function (d, i) {  }
    }
});
    var pieChart2 = c3.generate({
    bindto: '#pieChart2',
    data: {
        // iris data from R
        columns: [
            ['No User', 30]
        ],
        type : 'pie',
        onclick: function (d, i) {  },
        onmouseover: function (d, i) {  },
        onmouseout: function (d, i) {  }
    }
});
    var pieChart3 = c3.generate({
    bindto: '#pieChart3',
    data: {
        // iris data from R
        columns: [
            ['No Movie', 30]
        ],
        type : 'pie',
        onclick: function (d, i) { Session.set('selectedMovieRating',d.id) },
        onmouseover: function (d, i) {  },
        onmouseout: function (d, i) {  }
    }
});
    var pieChart4 = c3.generate({
    bindto: '#pieChart4',
    data: {
        // iris data from R
        columns: [
            ['No Movie', 30]
        ],
        type : 'pie',
        onclick: function (d, i) {  },
        onmouseover: function (d, i) {  },
        onmouseout: function (d, i) {  }
    }
});
    var pieChart5 = c3.generate({
    bindto: '#pieChart5',
    data: {
        // iris data from R
        columns: [
            ['No Movie', 30]
        ],
        type : 'pie',
        onclick: function (d, i) {  },
        onmouseover: function (d, i) {  },
        onmouseout: function (d, i) {  }
    }
});
    Deps.autorun(function() {

    })
    Deps.autorun(function(){
//         var iconsData = [
//   {
//     // the icon's latitude
//     lat: 44.980378999999999223,

//     // the icon's longitude
//     lng: -93.230029999999999291,
//     cssClass: 'datamap-icon-5'
//   }
// ];

//     map.icons(iconsData);

    })
    Deps.autorun(function(){
        var selectedMovieId=Session.get('movieSelectedForPieChart')
        if(selectedMovieId!==null){
        var movieScoreForPieChart_handler=Meteor.subscribe('movieScoreForPieChart',selectedMovieId)
        var zipcode_handler=Meteor.subscribe('zipcode')
        if(movieScoreForPieChart_handler.ready()&&zipcode_handler.ready()){
            var pieChartData=Collection.movieScoreForPieChart.find().fetch()[0];
            var zipcode=Collection.zipcode.find().fetch()
            var display3=[]
            Session.set('selectedMovie',pieChartData)
            display3.push(['1',pieChartData['1']['zip'].length])
            display3.push(['2',pieChartData['2']['zip'].length])
            display3.push(['3',pieChartData['3']['zip'].length])
            display3.push(['4',pieChartData['4']['zip'].length])
            display3.push(['5',pieChartData['5']['zip'].length])
            pieChart3.load({
            columns: display3,
            colors: {
            '1': '#ea0001',
            '2': '#fa9924',
            '3': '#feC923',
            '4': '#92d14f',
            '5': '#00602b'
        },
            unload:['No Movie']
                });
            var iconsData=[]
            for(var i=0;i<pieChartData['1']['zip'].length;i++){
                for (var j=0;j<zipcode.length;j++){
                    if (pieChartData['1']['zip'][i]===zipcode[j]['zip']){
                        iconsData.push({lat:zipcode[j]['latitude'],lng:zipcode[j]['longitude'],cssClass:'datamap-icon-1'})
                    }
                }
            }
            for(var i=0;i<pieChartData['2']['zip'].length;i++){
                for (var j=0;j<zipcode.length;j++){
                    if (pieChartData['2']['zip'][i]===zipcode[j]['zip']){
                        iconsData.push({lat:zipcode[j]['latitude'],lng:zipcode[j]['longitude'],cssClass:'datamap-icon-2'})
                    }
                }
            }
            for(var i=0;i<pieChartData['3']['zip'].length;i++){
                for (var j=0;j<zipcode.length;j++){
                    if (pieChartData['3']['zip'][i]===zipcode[j]['zip']){
                        iconsData.push({lat:zipcode[j]['latitude'],lng:zipcode[j]['longitude'],cssClass:'datamap-icon-3'})
                    }
                }
            }
            for(var i=0;i<pieChartData['4']['zip'].length;i++){
                for (var j=0;j<zipcode.length;j++){
                    if (pieChartData['4']['zip'][i]===zipcode[j]['zip']){
                        iconsData.push({lat:zipcode[j]['latitude'],lng:zipcode[j]['longitude'],cssClass:'datamap-icon-4'})
                    }
                }
            }
            for(var i=0;i<pieChartData['5']['zip'].length;i++){
                for (var j=0;j<zipcode.length;j++){
                    if (pieChartData['5']['zip'][i]===zipcode[j]['zip']){
                        iconsData.push({lat:zipcode[j]['latitude'],lng:zipcode[j]['longitude'],cssClass:'datamap-icon-5'})
                    }
                }
            }
            map.icons(iconsData);
            }

        }
        
    })
    Deps.autorun(function(){
        var occupationList=['administrator', 'writer', 'retired', 'student', 'entertainment', 'marketing', 'executive', 'none', 'scientist', 'healthcare', 'other', 'lawyer', 'educator', 'technician', 'librarian', 'programmer', 'artist', 'salesman', 'doctor', 'homemaker', 'engineer']
        var selectedMovie=Session.get('selectedMovie')
        var selectedMovieRating=Session.get('selectedMovieRating')
        if(selectedMovie!==null && selectedMovieRating!=null){
            var totalMaleScore=selectedMovie['1']['sex']['M']+selectedMovie['2']['sex']['M']+selectedMovie['3']['sex']['M']+selectedMovie['4']['sex']['M']+selectedMovie['5']['sex']['M']
            var totalFemaleScore=selectedMovie['1']['sex']['F']+selectedMovie['2']['sex']['F']+selectedMovie['3']['sex']['F']+selectedMovie['4']['sex']['F']+selectedMovie['5']['sex']['F']
            pieChart4.load({
        columns: [['Male',selectedMovie[selectedMovieRating]['sex']['M']/totalMaleScore],['Female',selectedMovie[selectedMovieRating]['sex']['F']/totalFemaleScore]],
        unload:['No Movie']
            });
            var display5=[]
            var display5Unload=['No Movie']
            for(var i=0;i<occupationList.length;i++){
                var score=selectedMovie[selectedMovieRating]['occupation'][occupationList[i]]
                if (score>0){
                display5.push([occupationList[i],score])
            }else{
                display5Unload.push(occupationList[i])
            }
            }
            pieChart5.load({
            columns: display5,
            unload: display5Unload
                });
        }
            
            
        
    })
    Deps.autorun(function(){
         var genreList=['Action','Adventure','Animation',"Children's",'Comedy','Crime','Documentary','Drama','Fantasy','Film-Noir','Horror','Musical','Mystery','Romance','Sci-Fi','Thriller','War','Western']
        var selectedRating=Session.get('selectedRating')
        if (selectedRating!==null){
        var userId=Session.get('userSelectedForPieChart')
        var userScoreForPieChart_handler=Meteor.subscribe('userScoreForPieChart',userId,selectedRating)
        if(userScoreForPieChart_handler.ready()){
            var pieChartData=Collection.userScoreForPieChart.find({}).fetch();
            //console.log(pieChartData)
            var display=[]
            var displayUnload=['No User']
            for(var i=0;i<genreList.length;i++){
                if (pieChartData[0][parseInt(selectedRating)][genreList[i]]>0){
                display.push([genreList[i],pieChartData[0][parseInt(selectedRating)][genreList[i]]])
            }else{
                displayUnload.push(genreList[i])
            }
            }
            console.log(display)
                pieChart2.load({
        columns: display,
        unload: displayUnload
            });
        }

    }

    })
     Deps.autorun(function() {
        var userId=Session.get('userSelectedForPieChart')
        if (userId!==null){
            var ratings=Session.get('ratings').filter(function(d){
                return d.userId===userId
            })
            var one=['1',ratings.filter(function(d){
                return d.rating ===1
            }).length]
            var two=['2',ratings.filter(function(d){
                return d.rating ===2
            }).length]
            var three=['3',ratings.filter(function(d){
                return d.rating ===3
            }).length]
            var four=['4',ratings.filter(function(d){
                return d.rating ===4
            }).length]
            var five=['5',ratings.filter(function(d){
                return d.rating ===5
            }).length]
            var data=[]
            data.push(one)
            data.push(two)
            data.push(three)
            data.push(four)
            data.push(five)
            pieChart1.load({
        columns: data,
        colors: {
            '1': '#ea0001',
            '2': '#fa9924',
            '3': '#feC923',
            '4': '#92d14f',
            '5': '#00602b'
        },
        unload:['No User']
    });
        }
    })
    Deps.autorun(function(){
        var ratings_handler=Meteor.subscribe('ratings')
        var items_handler=Meteor.subscribe('items')
        var user_handler=Meteor.subscribe('user')
        var genres=Session.get('genre')
        var userClassifier=Session.get('userClassifier')
        var userClassifierGroup=Session.get('userClassifierGroup')
        if(ratings_handler.ready()&&items_handler.ready()&&user_handler.ready()){
            $('#bipartiteCanvas').empty()
            var ratings=Collection.ratings.find().fetch();
            var items=Collection.items.find().fetch();
            var user=Collection.user.find().fetch();
            Session.set('ratings',ratings);
            Session.set('items',items);
            Session.set('user',user);
            var itemsV=items.filter(function(d){
                for(var i=0;i<genres.length;i++){
                    if (d[genres[i]]===0){
                        return false
                    }
                }
                return true;
            })
            itemsV.sort(function (a,b) {
                return Date.parse(b.releaseDate)-Date.parse(a.releaseDate)
                })
            console.log(itemsV)
            var user1=user.filter(function(d){
                return d[userClassifier]===userClassifierGroup[0]
            })
            user1.sort(function (a,b){
                return a.age-b.age
            })
            var user2=user.filter(function(d){
                return d[userClassifier]===userClassifierGroup[1]
            })
            user2.sort(function (a,b){
                return a.age-b.age
            })
            d3.select("#biGraphSvg").remove();
            bipartite('#bipartiteCanvas',$('#bipartiteCanvas').width(),$('#bipartiteCanvas').width()*0.6,itemsV,ratings,user1,user2)
            Session.set('biGraphIsReady',true)
        }
    })
    Deps.autorun(function(){
        if(Session.get('biGraphIsReady')){
        var scoreFilter = Session.get('scoreFilter');
        var svg = d3.select("#bipartiteCanvas").transition();
        svg.selectAll(".trendline") 
        .attr("opacity",function(d){
            //console.log(d)
            if (scoreFilter.indexOf(d['rating'])>-1){
                return 0.05
            }else{
                return 0
            }
        });
    }
    })
    Deps.autorun(function(){
        scatterplot('#scatterplotContainer',$('#scatterplotContainer').width(),$('#scatterplotContainer').width()*0.5)
    })

}

var checkScoreFilter = function(i) {
    var scoreFilter = Session.get('scoreFilter');
    var idx = scoreFilter.indexOf(i);
    if (idx === -1) {
        scoreFilter.push(i);
    } else {
        scoreFilter.splice(idx, 1);
    }
    Session.set('scoreFilter', scoreFilter);
}
var checkGenreFilter = function(i) {
    var scoreFilter = Session.get('genre');
    var idx = scoreFilter.indexOf(i);
    if (idx === -1) {
        scoreFilter.push(i);
    } else {
        scoreFilter.splice(idx, 1);
    }
        $('#score_one').removeClass('active');
        $('#score_two').removeClass('active');
        $('#score_three').removeClass('active');
        $('#score_four').removeClass('active');
        $('#score_five').removeClass('active');
    Session.set('genre', scoreFilter);
    Session.set('scoreFilter', [])
}
var uncheckAllOtherBoxes=function(checked){
    var allGenres=["#action", "#adventure" ,"#animation","#children's","#comedy","#crime","#documentary","#drama","#fantasy","#filmNoir","#horror","#musical","#mystery","#romance","#sciFi","#thriller","#war","#western"]
    for (var i=0;i<allGenres.length;i++){
        if (allGenres[i]!==checked){
    $(allGenres[i]).button('reset')
    Session.set('scoreFilter', [])
}
}
}
Template.mainview.helpers({
    'selectedId': function(){
        var id=Session.get('userSelectedForPieChart');
        var userOccupation=Session.get('userOccupationSelectedForPieChart')
        if(id&&userOccupation){
            return id+' ('+userOccupation+')';
        }else{
            return 'none';
        }
    },
    'movieId': function(){
        var id=Session.get('movieSelectedForPieChart');
        if(id){
            return id;
        }else{
            return 'none';
        }
    },
    'movieTitle': function(){
        var id=Session.get('movieSelectedForPieChart');
        var item=Session.get('items');
        if (item.length<=0){
            return 'none'
        }
        if(id){
            for(var i=0;i<item.length;i++){
                if (item[i]['movieId']===id){
                    return item[i]['movieTitle']
                }
            }
            return 'none';
        }else{
            return 'none';
        }
    },'movieReleaseDate': function(){
        var id=Session.get('movieSelectedForPieChart');
        var item=Session.get('items')
        if (item.length<=0){
            return 'none'
        }
        if(id){
            for(var i=0;i<item.length;i++){
                if (item[i]['movieId']===id){
                    return item[i]['releaseDate']
                }
            }
            return 'none';
        }else{
            return 'none';
        }
    },'movieGenre': function(){
        var id=Session.get('movieSelectedForPieChart');
        var item=Session.get('items')
        if (item.length<=0){
            return 'none'
        }
        if(id){
            for(var i=0;i<item.length;i++){
                if (item[i]['movieId']===id){
                    var genre=''
                    var genreList=['Action','Adventure','Animation',"Children's",'Comedy','Crime','Documentary','Drama','Fantasy','Film-Noir','Horror','Musical','Mystery','Romance','Sci-Fi','Thriller','War','Western']
                    for (var j=0; j<genreList.length;j++){
                        if (item[i][genreList[j]]>0){
                           genre=genre+genreList[j]+' | ' 
                        }
                    }
                    console.log(genre)
                    return genre
                }
            }
            return 'none';
        }else{
            return 'none';
        }
    }
})

Template.mainview.events({
    'click #score_one': function(event) {
        checkScoreFilter(1)
    },
    'click #score_two': function(event) {
        checkScoreFilter(2)
    },
    'click #score_three': function(event) {
        checkScoreFilter(3)
    },
    'click #score_four': function(event) {
        checkScoreFilter(4)
    },
    'click #score_five': function(event) {
        checkScoreFilter(5)
    },

    'click #action': function(event) {
        checkGenreFilter('Action')
        //uncheckAllOtherBoxes("#action")

    },
    'click #adventure': function(event) {
       checkGenreFilter('Adventure')
        //uncheckAllOtherBoxes("#adventure")
    },
    'click #animation': function(event) {
        checkGenreFilter('Animation')
        //uncheckAllOtherBoxes("#animation")
    },
    'click #children': function(event) {
        checkGenreFilter("Children's")
        //uncheckAllOtherBoxes("#children")
    },
    'click #comedy': function(event) {
        checkGenreFilter('Comedy')
        //uncheckAllOtherBoxes("#comedy")
    },

    'click #crime': function(event) {
        checkGenreFilter('Crime')
        //uncheckAllOtherBoxes("#crime")
    },
    'click #documentary': function(event) {
        checkGenreFilter('Documentary')
        //uncheckAllOtherBoxes("#documentary")
    },
    'click #drama': function(event) {
        checkGenreFilter('Drama')
        //uncheckAllOtherBoxes("#drama")
    },
    'click #fantasy': function(event) {
        checkGenreFilter('Fantasy')
        //uncheckAllOtherBoxes("#fantasy")
    },
    'click #filmNoir': function(event) {
        checkGenreFilter('Film-Noir')
        //uncheckAllOtherBoxes("#filmNoir")
    },

    'click #horror': function(event) {
        checkGenreFilter('Horror')
        //uncheckAllOtherBoxes("#horror")
    },
    'click #musical': function(event) {
        checkGenreFilter('Musical')
        //uncheckAllOtherBoxes("#musical")
    },
    'click #mystery': function(event) {
        checkGenreFilter('Mystery')
        //uncheckAllOtherBoxes("#mystery")
    },
    'click #romance': function(event) {
        checkGenreFilter('Romance')
        //uncheckAllOtherBoxes("#romance")
    },
    'click #sciFi': function(event) {
        checkGenreFilter('Sci-Fi')
        //uncheckAllOtherBoxes("#sciFi")
    },

    'click #thriller': function(event) {
        checkGenreFilter('Thriller')
        //uncheckAllOtherBoxes("#thriller")
    },
    'click #war': function(event) {
        checkGenreFilter('War')
        //uncheckAllOtherBoxes("#war")
    },
    'click #western': function(event) {
        checkGenreFilter('Western')
        //uncheckAllOtherBoxes("#western")
    },
    'click #occupation': function(event) {
        Session.set('userClassifier','occupation')
        Session.set('userClassifierGroup',['administrator','artist'])
        Session.set('scoreFilter', [])
        $('#occupationSelector1').val('administrator')
        $('#occupationSelector2').val('artist')
        $('#score_one').removeClass('active');
        $('#score_two').removeClass('active');
        $('#score_three').removeClass('active');
        $('#score_four').removeClass('active');
        $('#score_five').removeClass('active');
    },
    'click #gender': function(event) {
        Session.set('userClassifier','gender')
        Session.set('userClassifierGroup',['M','F'])
        Session.set('scoreFilter', [])
        $('#score_one').removeClass('active');
        $('#score_two').removeClass('active');
        $('#score_three').removeClass('active');
        $('#score_four').removeClass('active');
        $('#score_five').removeClass('active');
    },

    'change #occupationSelector1': function(event) {
        var result=Session.get('userClassifierGroup')
        var currentSelection=Session.get('userClassifier')
        var val = $(event.target).val();
        result[0]=val
        if(currentSelection==='occupation'){
            Session.set('userClassifierGroup',result)
        $('#score_one').removeClass('active');
        $('#score_two').removeClass('active');
        $('#score_three').removeClass('active');
        $('#score_four').removeClass('active');
        $('#score_five').removeClass('active');
        Session.set('scoreFilter', [])
        }
    },
    'change #occupationSelector2': function(event) {
         var result=Session.get('userClassifierGroup')
        var currentSelection=Session.get('userClassifier')
        var val = $(event.target).val();
        result[1]=val
        if(currentSelection==='occupation'){
            Session.set('userClassifierGroup',result)
        $('#score_one').removeClass('active');
        $('#score_two').removeClass('active');
        $('#score_three').removeClass('active');
        $('#score_four').removeClass('active');
        $('#score_five').removeClass('active');
        Session.set('scoreFilter', [])
        }
    },
    // 'mouseover #map_score_one':function(event){
    //     for(var i=1;i<6;i++){
    //     if (i!==1){
    //     var cols = document.getElementsByClassName('datamap-icon-'+i.toString());
    //       for(i=0; i<cols.length; i++) {
    //         cols[i].style.opacity = 0;
    //       }
    //   }
    //   }
    // },
    // 'mouseout #map_score_one':function(event){
    //     for(var i=1;i<6;i++){
    //     if (i!==1){
    //     var cols = document.getElementsByClassName('datamap-icon-'+i.toString());
    //       for(i=0; i<cols.length; i++) {
    //         cols[i].style.opacity = 0.3;
    //       }
    //   }
    //   }
    // },
    // 'mouseover #map_score_two':function(event){

    // },
    // 'mouseout #map_score_two':function(event){

    // },
    // 'mouseover #map_score_three':function(event){

    // },
    // 'mouseout #map_score_three':function(event){

    // },
    // 'mouseover #map_score_four':function(event){

    // },
    // 'mouseout #map_score_four':function(event){

    // },
    // 'mouseover #map_score_five':function(event){

    // },
    // 'mouseout #map_score_five':function(event){

    // }

});

Meteor.startup(function() {
    Session.set('scoreFilter', [])
    Session.set('genre',['Action'])
    Session.set('biGraphIsReady',false)
    Session.set('userClassifier','gender')
    Session.set('userClassifierGroup',['M','F'])
    Session.set('ratings',[])
    Session.set('user',[])
    Session.set('items',[])
    Session.set('userSelectedForPieChart',null)
    Session.set('movieSelectedForPieChart',null)
    Session.set('selectedRating',null)
    Session.set('selectedMovieRating',null)
    Session.set('selectedMovie',null)
    Session.set('userOccupationSelectedForPieChart',null)
    /*
    Session.set('mdsFilterVisitors', [])
    Session.set('date', 0)
    Session.set('selectedTime', undefined)
    Session.set('mouseTrack', undefined)
    Session.set('mouseTS', undefined)
    */
})
