var Collection = {};
Collection.ratings = new Meteor.Collection('ratings')
Collection.items= new Meteor.Collection('items')
Collection.user= new Meteor.Collection('user')
Collection.userScoreForPieChart= new Meteor.Collection('userScoreForPieChart')
Collection.movieScoreForPieChart= new Meteor.Collection('movieScoreForPieChart')
Collection.zipcode= new Meteor.Collection('zipcode')

Meteor.publish('zipcode',function(){
    return Collection.zipcode.find()
})
Meteor.publish('movieScoreForPieChart',function(movieId){
    return Collection.movieScoreForPieChart.find({'movieId':movieId},{})
})
Meteor.publish('userScoreForPieChart',function(userId,score){
    return Collection.userScoreForPieChart.find({'userId':userId},{score:1})
})
Meteor.publish('ratings',function(){
    return Collection.ratings.find()
})
Meteor.publish('items',function(){
    return Collection.items.find()
})
Meteor.publish('user',function(){
    return Collection.user.find()
})
/*
Collection.mc1 = new Meteor.Collection('mc1')
Collection.mds = new Meteor.Collection('mds')
Collection.timeline = new Meteor.Collection('timeline')
Collection.lineChart = new Meteor.Collection('lineChart')
Collection.timeInterval = new Meteor.Collection('timeInterval')
Collection.checkinLineChart = new Meteor.Collection('checkinLineChart')
Collection.mdsFilter = new Meteor.Collection('mdsFilter')
Collection.mdsConn = new Meteor.Collection('mdsConn')
Collection.mdsTianyu = new Meteor.Collection('mdsTianyu')
Collection.mdsFromConn = new Meteor.Collection('mdsFromConn')
Collection.checkinById = new Meteor.Collection('checkinById')
Collection.mc2 = new Meteor.Collection('mc2')
Collection.mdscat = new Meteor.Collection('mds6cat') //useless
Collection.heatmapPerDay = new Meteor.Collection('heatmapPerDay');
Collection.heatmap_ids = new Meteor.Collection('heatmap_ids');
Collection.inout = new Meteor.Collection('inout')
Collection.friTheme = new Meteor.Collection('friTheme')
Collection.satTheme = new Meteor.Collection('satTheme')
Collection.sunTheme = new Meteor.Collection('sunTheme')

Meteor.publish('checkinById', function(Day, idArr) {
    return Collection.checkinById.find({
        Day: Day,
        id: {
            $in: idArr
        }
    });
})

Meteor.publish('connToArr', function(day, idArr) {
    console.log(day, idArr);
    return Collection.mdsConn.find({
        day: day,
        id: {
            $in: idArr
        }
    });
})

Meteor.publish('connFromArr', function(day, idArr) {
    return Collection.mdsFromConn.find({
        day: day,
        id: {
            $in: idArr
        }
    });
})

Meteor.publish('connToStat',function(day){
    console.log(day)
    return Collection.mdsConn.find({
        day: day
    });
})

Meteor.publish('connFromStat',function(day){
    return Collection.mdsFromConn.find({
        day: day
    });
})

var dayConst = ['2014-6-06 08:00:00', '2014-6-07 08:00:00', '2014-6-08 08:00:00', '2014-6-09 08:00:00']
var str2sec = function(str) {
    var inputFormat = 'YYYY-M-DD HH:mm:ss'
    var parse = moment(str, inputFormat);
    return parse.hour() * 3600 + parse.minute() * 60 + parse.second();
}

var secondsToTime = function(secs) {
    var hour = Math.floor(secs / (60 * 60));

    var divisor_for_minutes = secs % (60 * 60);
    var minute = Math.floor(divisor_for_minutes / 60);

    var divisor_for_seconds = divisor_for_minutes % 60;
    var second = Math.ceil(divisor_for_seconds);

    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    return '' + hour + ':' + minute + ':' + second;
}

Meteor.publish('singlePath', function(id) {
    return Collection.mc1.find({
        id: id
    });
})

Meteor.publish('mds', function(day) {
    return Collection.mds.find({
        day: day,
    });
})

Meteor.publish('mdsTianyu', function(day) {
    return Collection.mdsTianyu.find({
        day: day,
    });
})

Meteor.publish('mdscat', function(category) {
    return Collection.mdscat.find({});
})

Meteor.publish('mc2', function(day, id) {
    var istart, iend;
    istart = dayConst[day];
    istart = dayConst[day + 1];

    return Collection.mc2.find({
        Timestamp: {
            $gt: istart,
            $lt: iend
        },
        from: id
    });
})
Meteor.publish('mdscat2', function() {
    return Collection.mdscat.find({});
})
Meteor.publish('timePoint', function(time) {
    return Collection.heatmapPerDay.find({
        time: time
    });
})

Meteor.publish('heatmap_ids', function(time) {
    console.log(time)
    return Collection.heatmap_ids.find({
            time: time
        })
})

Meteor.publish('populationCoord', function(coordStart, coordEnd, istart, iend) {
    if (coordStart[0] > coordEnd[0]) {
        var lowX = coordEnd[0];
        var highX = coordStart[0];
    } else {
        var lowX = coordStart[0];
        var highX = coordEnd[0];
    }
    if (coordStart[1] > coordEnd[1]) {
        var lowY = coordEnd[1];
        var highY = coordStart[1];
    } else {
        var lowY = coordStart[1];
        var highY = coordStart[1];
    }
    return Collection.mc1.find({
        Timestamp: {
            $gt: istart,
            $lt: iend
        },
        X: {
            $gt: lowX,
            $lt: highX
        },
        Y: {
            $gt: lowY,
            $lt: highY
        }
    })
})
Meteor.publish('timeline', function(day) {
    var dateConst = ['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']
    return Collection.timeline.find({
        time: {
            $gt: dateConst[day],
            $lt: dateConst[day + 1]
        }
    })
})


Meteor.publish('timeInterval', function(day, timeInterval) {
    return Collection.timeInterval.find({
        "Day": day,
        "time": {
            $gte: timeInterval[0],
            $lte: timeInterval[1]
        }
    }, {
        fields: {
            'ids': 1
        }
    })
})

Meteor.publish('mdsFilter', function(day, coord) {
    var dateConst = ['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']
    return Collection.mdsFilter.find({
        Day: day,
        $or: coord


    }, {
        fields: {
            'ids': 1
        }
    })
});

Meteor.publish('lineChart', function(day) {
    var dateConst = [1402041600000, 1402099200001, 1402185600001, 1402272000001]
    return Collection.lineChart.find({
        time2: {
            $gte: dateConst[day],
            $lt: dateConst[day + 1]
        },
        Minute: {
            $in: [0, 15, 30, 45]
        },
    }, {
        fields: {
            'Traffic': 1,
            'conn': 1
        }
    })
})

Meteor.publish('inout', function(day) {
    return Collection.inout.find({
        day: day,
    }, {
        sort: {
            'order': 1
        }
    })
})

Meteor.publish('friTheme', function() {

    return Collection.friTheme.find({});

})

Meteor.publish('satTheme', function() {

    return Collection.satTheme.find({});

})
Meteor.publish('sunTheme', function() {

    return Collection.sunTheme.find({});
})

Meteor.publish('checkinLineChart', function(day, coord) {
    console.log(day);
    console.log(coord);
    return Collection.checkinLineChart.find({
        'Day': day,
        'X': coord[0],
        'Y': coord[1]
    }, {
        fields: {
            'Traffic': 1,
            'time': 1
        }
    }, {
        sort: {
            'time': 1
        }
    })
})
*/