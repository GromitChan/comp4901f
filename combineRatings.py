from pymongo import MongoClient
client = MongoClient('localhost',3001)
db = client.meteor
genreList=[u'Action','Adventure','Animation',"Children's",'Comedy','Crime','Documentary','Drama','Fantasy','Film-Noir','Horror','Musical','Mystery','Romance','Sci-Fi','Thriller','War','Western']

user=[]
items=[]
ratings=[]
cursor=db.user.find()
for val in cursor:
	user.append(val)
cursor=db.items.find()
for val in cursor:
	items.append(val)
cursor=db.ratings.find()
for val in cursor:
	ratings.append(val)
for val in user:
	ratingOne=[x['itemId'] for x in ratings if ((x['rating']==1) and (x['userId']==val['userId']))]
	scoreOne={}
	for genre in genreList:
		scoreOne[genre]=0
	for movie in ratingOne:
		search=(item for item in items if item["movieId"] == movie).next()
		for genre in genreList:
			scoreOne[genre]+=search[genre]

	ratingTwo=[x['itemId'] for x in ratings if ((x['rating']==2) and (x['userId']==val['userId']))]
	scoreTwo={}
	for genre in genreList:
		scoreTwo[genre]=0
	for movie in ratingTwo:
		search=(item for item in items if item["movieId"] == movie).next()
		for genre in genreList:
			scoreTwo[genre]+=search[genre]

	ratingThree=[x['itemId'] for x in ratings if ((x['rating']==3) and (x['userId']==val['userId']))]
	scoreThree={}
	for genre in genreList:
		scoreThree[genre]=0
	for movie in ratingThree:
		search=(item for item in items if item["movieId"] == movie).next()
		for genre in genreList:
			scoreThree[genre]+=search[genre]

	ratingFour=[x['itemId'] for x in ratings if ((x['rating']==4) and (x['userId']==val['userId']))]
	scoreFour={}
	for genre in genreList:
		scoreFour[genre]=0
	for movie in ratingFour:
		search=(item for item in items if item["movieId"] == movie).next()
		for genre in genreList:
			scoreFour[genre]+=search[genre]

	ratingFive=[x['itemId'] for x in ratings if ((x['rating']==5) and (x['userId']==val['userId']))]
	scoreFive={}
	for genre in genreList:
		scoreFive[genre]=0
	for movie in ratingFive:
		search=(item for item in items if item["movieId"] == movie).next()
		for genre in genreList:
			scoreFive[genre]+=search[genre]

	upload={
	'1':scoreOne,
	'2':scoreTwo,
	'3':scoreThree,
	'4':scoreFour,
	'5':scoreFive,
	'userId':val['userId']
	}
	db.userScoreForPieChart.insert(upload)
